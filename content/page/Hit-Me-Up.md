---
title: "Talk to me"
date: 2020-12-03T12:40:25-05:00
draft: false
---


**YOU**, yes you, can talk to me at

The best way -
[Matrix](https://matrix.to/#/@grayson:destinationlinux.network)

[Email](mailto:thatcomputerkid@pm.me)

Geekshed & ~~Freenode~~ Libera.chat: ComputerKid

[Discord](https://discord.com/channels/computerkid#6125)

[Telegram](https://telegram.me/computerkid)
