---
title: "About Me"
date: 2020-05-28T20:34:07Z
draft: false
---

My current Tech Setup:
- A Tuxedo Aura 15, runnning Fedora KDE spin.
- A Dell Optiplex 3010 with Ubuntu 20.04 where I run all my many Docker containers!
- Three more laptops used for playing around

For Audio, I'm keeping it simple: 
- Samson Q2U
- The best $5 earbuds on amazon 
