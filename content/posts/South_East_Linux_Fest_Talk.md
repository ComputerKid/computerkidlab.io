---
title: "South East Linux Fest Talk"
date: 2020-06-13T19:32:49Z
draft: false
tags: ['Conference', 'South East Linux Fest',]
author: "ComputerKid"
---

## I gave a talk at the South East Linux Fest virtual event and dropped my slides here


My presentation: [https://docs.google.com/presentation/d/17atRgu4vf_XqGVom1GoWuDePo_7pi-Pt8PSyJHTIqYA/edit?usp=sharing](https://docs.google.com/presentation/d/17atRgu4vf_XqGVom1GoWuDePo_7pi-Pt8PSyJHTIqYA/edit?usp=sharing)

Yes I know I used Google, I'm sorry.

Please pardon my bad notes


## Some Credits

A Video I stole stuff from [https://youtu.be/bBHVlqCK96w](https://youtu.be/bBHVlqCK96w)

There are some smart and cool people in here who helped me: [https://discord.gg/jw4aJh](https://discord.gg/jw4aJh)

This News site helped me [https://itsfoss.com/](https://itsfoss.com/)

And always Linux Delta [https://linuxdelta.com](https://linuxdelta.com)
