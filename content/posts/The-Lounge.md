---
title: "The Lounge IRC"
date: 2020-05-29T21:06:00Z
draft: false
tags: ['Docker', 'IRC',]
author: "ComputerKid"


# set the link if you want to redirect the user.
link: ""
# set the html target parameter if you want to change default behavior
target: "_blank"
---

[The Lounge IRC client](https://thelounge.chat/) is a self-hosted web IRC client. Meaning an IRC client that will run on your server or always connected computer so that it never misses a message. You can then connect to The Lounge through any web browser and read those messages. Becuase it is in a browser it has great mobile support, one of the only decent mobile IRC cleints I have found
 It is well designed, has the features of most modern chat client like emojis, searching, looking back at where people have pinged you, a great mobile interface etc.

I'm going to tell you how I set it up if you don't mind.

I use Docker to run most of my self hosted services and [Linux Server](linuxserver.io) has some great ones has including one for The Lounge. So assuming you have [Docker installed](https://docs.docker.com/engine/install/), create a file like `thelounge.sh` and enter in the following:

```
  sudo docker run -d \
    --name thelounge \
    --publish 80:9000 \
    --volume ~/thelounge-data:/config \
    --restart always \
    linuxserver/thelounge
```

## Breakdown:
| Command                                      | Meaning                                                                                 |
| -------------------------------------------- | --------------------------------------------------------------------------------------- |
| `sudo docker run -d`                           | Means we want to run a container in a detached mode                                     |
| `--name thelounge`                             | Names this container 'thelounge'                                                        |
| `--publish 80:9000`                            | Let's it publish the web page on port 80 of the host                                    |
| `--volume ~/thelounge-data:/var/opt/thelounge` | Saves the data from /config in the container to ~/thelounge-data on the host |
| `--restart always`                             | Means that it will always restart unless deleted                                        |
| `linuxserver/thelounge`                        | identifies which container we want to use

Run `chmod +xr thelounge.sh` and `./thelounge.sh`

This will make a directory in your home directory called `thelounge-data`. This directory holds your config file(`config.js`) for The Lounge. I recommend you read and edit this config to your liking. Unless you plan to have strangers using your Lounge IRC instance, you probably want to change `public: true,` to `public: false,`. Once you have all that finished, you can add your user to The Lounge by running `docker exec --user root -it //thelounge thelounge add YoUrUsErNaMe` Then it will ask you for a password.
Make sure you change YoUrUsErNaMe to something more logical :)

You should now be good to go!

Visit `192.168.X.X` replacing the X with your server's IP, and if all goes well you should see it.
