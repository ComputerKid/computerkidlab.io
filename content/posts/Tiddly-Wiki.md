---
title: "Tiddly Wiki"
date: 2020-05-29T21:37:14Z
draft: true
tags: ['npm', 'node', 'tiddlywiki']
author: "ComputerKid"
---

I plan to use this website as a place to dump thoughts that are acceptable for the internet. But on occasion there are things I want to record purely for myself. For that I use app called TiddlyWiki.

There are many ways that TiddlyWiki could be run but
